CI_REGISTRY_IMAGE ?= registry.gitlab.com/master20/pandoc-docker
CI_COMMIT_REF_SLUG ?= latest
CONTAINER_RELEASE_IMAGE ?= ${CI_REGISTRY_IMAGE}:latest
CONTAINER_TEST_IMAGE ?= ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}

.PHONY: all
all: $(final_artifacts)

.PHONY: clean
clean:
	docker rmi ${CONTAINER_TEST_IMAGE} || true
	docker rmi ${CONTAINER_RELEASE_IMAGE} || true

.PHONY: build
build:
	docker build \
		--cache-from=archlinux:latest,${CONTAINER_RELEASE_IMAGE} \
		-f Dockerfile \
		-t ${CONTAINER_TEST_IMAGE} \
		.

.PHONY: pull
pull:
	docker pull ${CONTAINER_TEST_IMAGE}
