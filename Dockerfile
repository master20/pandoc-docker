#################################################
# From https://github.com/listx/texlive-docker
# Copied under permissions granted by the
# BSD-2-Clause License
#################################################

FROM archlinux:latest

# Find nearest and fastest mirror
COPY docker/mirrorlist /etc/pacman.d/mirrorlist
RUN pacman -Syw \
    && pacman -S --noconfirm grep \
    && pacman -Scc --noconfirm

RUN \
    # sync database
    pacman -Syw \
    #################################################
    # Archlinux packages
    #################################################
    && pacman -S --noconfirm \
    base-devel \
    biber \
    # texlive and fonts
    texlive-most \
    # texlive-lang \
    # install fonts
    adobe-source-code-pro-fonts \
    adobe-source-sans-pro-fonts \
    adobe-source-serif-pro-fonts \
    noto-fonts \
    noto-fonts-emoji \
    noto-fonts-extra \
    otf-fira-code \
    otf-fira-mono \
    otf-fira-sans \
    ttf-dejavu \
    ttf-droid \
    ttf-lato \
    ttf-opensans \
    ttf-roboto \
    ttf-roboto-mono \
    ttf-ubuntu-font-family \
    # install git
    git \
    # haskell stuff
    cabal-install \
    ghc \
    # pandoc
    pandoc \
    pandoc-citeproc \
    pandoc-crossref \
    ghostscript \
    # python
    python \
    python-setuptools \
    python-wheel \
    python-pip \
    #################################################
    # python libraries
    #################################################
    && pip install \
    # https://github.com/tomduck/pandoc-xnos
    pandoc-fignos \
    pandoc-eqnos \
    pandoc-tablenos \
    pandoc-secnos \
    # https://github.com/DCsunset/pandoc-include
    pandoc-include \
    #################################################
    # Haskell Libraries
    #################################################
    # && cabal update \
    # && cabal install pandoc-include-code \
    #################################################
    # clear cache
    #################################################
    && pacman -Scc --noconfirm

#################################################
# define workdir
#################################################

WORKDIR /work

# We have to manually edit PATH so that the "biber" binary is readily available
# to us. Although biber itself is found only in /usr/bin/vendor_perl, we include
# other Perl-based paths for the sake of completeness.
# https://www.reddit.com/r/archlinux/comments/5ash5v/problem_with_biber/
ENV PATH="/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:${PATH}"
